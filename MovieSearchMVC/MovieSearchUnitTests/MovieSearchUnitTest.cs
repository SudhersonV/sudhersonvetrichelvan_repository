﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MovieSearch.Common.Abstract;
using MovieSearch.Common.Entities;
using System.Collections.Generic;
using MovieSearch.Web.Controllers;

namespace MovieSearchUnitTests
{
    [TestClass]
    public class MovieSearchUnitTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            Mock<ITitleDataSource> mock = new Mock<ITitleDataSource>();
            mock.Setup(m => m.GetGenreList()).Returns(new List<GenreModel>{ new GenreModel{ Id = 1, Name="Adaptation"}, 
                new GenreModel{Id = 2, Name="Drama"}});

            mock.Setup(m => m.GetTitleList("cas", "Drama")).Returns( 
                new List<TitleModel>{
                    new TitleModel{
                        TitleId = 1, TitleName = "Casablanca", TitleNameSortable = "Alternate Name", TitleTypeId = 0, ReleaseYear = 1942,
                        ProcessedDateTimeUTC = Convert.ToDateTime("6/15/2013 6:01:55 AM"), 
                        Awards = new List<AwardModel>{ 
                            new AwardModel{TitleId = 1, AwardWon = true, AwardYear = 1942,Award1 = "Globe", AwardCompany = "Globe", Id = 1
                        }}, 
                        OtherNames = new List<OtherNameModel>(), 
                        StoryLines = new List<StoryLineModel>(), 
                        TitleGenres = new List<TitleGenreModel> (),
                        TitleParticipants = new List<TitleParticipantModel>() 
                    }
                });

            MovieController target = new MovieController(mock.Object);
            //target.TitleListPartialView(mock.Object, "cas", "Drama").Model;
        }
    }
}
