﻿using MovieSearch.Common.Entities;
using MovieSearch.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieSearch.DAO
{
    public class MovieAdapter
    {
        TitlesEntities db = new TitlesEntities();

        public List<Title> GetTitle(string searchByContent, string searchByGenre)
        {
            List<Title> titles = new List<Title>();

            if (searchByContent.ToUpper() == "ALL" && searchByGenre.ToUpper() == "ALL")
                titles = db.Titles.ToList();
            else if (searchByContent.ToUpper() == "ALL" && searchByGenre.ToUpper() != "ALL")
                titles = db.Titles.Where(t => t.TitleGenres.Any(g => g.Genre.Name == searchByGenre)).ToList();
            else if (searchByContent.ToUpper() != "ALL" && searchByGenre.ToUpper() == "ALL")
                titles = db.Titles.Where(t => t.TitleName.Contains(searchByContent)).ToList();
            else
                titles = db.Titles.Where(t => t.TitleName.Contains(searchByContent) && t.TitleGenres.Any(g => g.Genre.Name == searchByGenre)).ToList();
            
            return titles;
        }

        public List<Genre> GetGenre()
        {
            List<Genre> GenreModel = new List<Genre>();
            GenreModel = db.Genres.ToList();
            return GenreModel;
        }
    }
}
