﻿using MovieSearch.Common.Entities;
using MovieSearch.Common.Enums;
using MovieSearch.DAO;
using MovieSearchWCF.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace MovieSearchWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Movie : IMovie
    {
        private MovieAdapter DAO;
        private CustomAutoMapper customAutoMapper;
        public Movie()
        {
            DAO = new MovieAdapter();
            customAutoMapper = new CustomAutoMapper();
        }

        public List<TitleModel> GetMovies(string searchByContent, string searchByGenre)
        {
            List<TitleModel> TitlesModel = new List<TitleModel>();
            List<Title> Titles = new List<Title>();

            Titles = DAO.GetTitle(searchByContent, searchByGenre);

            Titles.ForEach(x =>
            {
                TitlesModel.Add(customAutoMapper.MapObject<Title, TitleModel>(x));
            });

            return TitlesModel;
        }

        public List<GenreModel> GetGenres()
        {
            List<Genre> GenreList = DAO.GetGenre();
            List<GenreModel> GenreModel = new List<GenreModel>();

            GenreList.ForEach(g =>
            {
                GenreModel.Add(customAutoMapper.MapObject<Genre, GenreModel>(g));
            });

            return GenreModel;
        }
    }
}
