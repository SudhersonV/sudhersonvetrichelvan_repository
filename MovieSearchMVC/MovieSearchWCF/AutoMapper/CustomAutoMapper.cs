﻿using AutoMapper;
using MovieSearch.Common.Entities;
using MovieSearch.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieSearchWCF.AutoMapper
{
    public class CustomAutoMapper
    {
        public CustomAutoMapper()
        {
            Mapper.CreateMap<Title, TitleModel>()
                .ForMember(d => d.TitleId, op => op.MapFrom(t => t.TitleId))
                .ForMember(d => d.TitleName, op => op.MapFrom(t => t.TitleName))
                .ForMember(d => d.TitleNameSortable, op => op.MapFrom(t => t.TitleNameSortable))
                .ForMember(d => d.TitleTypeId, op => op.MapFrom(t => t.TitleTypeId))
                .ForMember(d => d.ReleaseYear, op => op.MapFrom(t => t.ReleaseYear))
                .ForMember(d => d.ProcessedDateTimeUTC, op => op.MapFrom(t => t.ProcessedDateTimeUTC))
                .ForMember(d => d.Awards, op => op.MapFrom(t => t.Awards))
                .ForMember(d => d.OtherNames, op => op.MapFrom(t => t.OtherNames))
                .ForMember(d => d.StoryLines, op => op.MapFrom(t => t.StoryLines))
                .ForMember(d => d.TitleGenres, op => op.MapFrom(t => t.TitleGenres))
                .ForMember(d => d.TitleParticipants, op => op.MapFrom(t => t.TitleParticipants))
                ;

            Mapper.CreateMap<Participant, ParticipantModel>()
                .ForMember(d => d.Id, op => op.MapFrom(t => t.Id))
                .ForMember(d => d.Name, op => op.MapFrom(t => t.Name))
                .ForMember(d => d.ParticipantType, op => op.MapFrom(t => t.ParticipantType))
                //.ForMember(d => d.TitleParticipants, op => op.MapFrom(t => t.TitleParticipants))
                ;

            Mapper.CreateMap<Genre, GenreModel>()
                .ForMember(d => d.Id, op => op.MapFrom(t => t.Id))
                .ForMember(d => d.Name, op => op.MapFrom(t => t.Name))
                //.ForMember(d => d.TitleGenres, op => op.MapFrom(t => t.TitleGenres))                
                ;

            Mapper.CreateMap<TitleParticipant, TitleParticipantModel>()
                .ForMember(d => d.Id, op => op.MapFrom(t => t.Id))
                .ForMember(d => d.IsKey, op => op.MapFrom(t => t.IsKey))
                .ForMember(d => d.IsOnScreen, op => op.MapFrom(t => t.IsOnScreen))
                .ForMember(d => d.ParticipantId, op => op.MapFrom(t => t.ParticipantId))
                .ForMember(d => d.RoleType, op => op.MapFrom(t => t.RoleType))
                .ForMember(d => d.TitleId, op => op.MapFrom(t => t.TitleId))
                //.ForMember(d => d.Participant, op => op.MapFrom(t => t.Participant))
                ;

            Mapper.CreateMap<TitleGenre, TitleGenreModel>()
                .ForMember(d => d.GenreId, op => op.MapFrom(t => t.GenreId))
                .ForMember(d => d.Id, op => op.MapFrom(t => t.Id))
                .ForMember(d => d.TitleId, op => op.MapFrom(t => t.TitleId))
                .ForMember(d => d.Genre, op => op.MapFrom(t => t.Genre))
                ;

            Mapper.CreateMap<StoryLine, StoryLineModel>()
                .ForMember(d => d.Id, op => op.MapFrom(t => t.Id))
                .ForMember(d => d.Language, op => op.MapFrom(t => t.Language))
                .ForMember(d => d.TitleId, op => op.MapFrom(t => t.TitleId))
                .ForMember(d => d.Type, op => op.MapFrom(t => t.Type))
                .ForMember(d => d.Description, op => op.MapFrom(t => t.Description))
                ;

            Mapper.CreateMap<Award, AwardModel>()
                .ForMember(d => d.Award1, op => op.MapFrom(t => t.Award1))
                .ForMember(d => d.AwardCompany, op => op.MapFrom(t => t.AwardCompany))
                .ForMember(d => d.AwardWon, op => op.MapFrom(t => t.AwardWon))
                .ForMember(d => d.AwardYear, op => op.MapFrom(t => t.AwardYear))
                .ForMember(d => d.Id, op => op.MapFrom(t => t.Id))
                .ForMember(d => d.TitleId, op => op.MapFrom(t => t.TitleId))
                ;

            Mapper.CreateMap<OtherName, OtherNameModel>()
                .ForMember(d => d.Id, op => op.MapFrom(t => t.Id))
                .ForMember(d => d.TitleId, op => op.MapFrom(t => t.TitleId))
                .ForMember(d => d.TitleName, op => op.MapFrom(t => t.TitleName))
                .ForMember(d => d.TitleNameLanguage, op => op.MapFrom(t => t.TitleName))
                .ForMember(d => d.TitleNameSortable, op => op.MapFrom(t => t.TitleNameSortable))
                .ForMember(d => d.TitleNameType, op => op.MapFrom(t => t.TitleNameType))
                ;
        }

        public TDestination MapObject<TSource, TDestination>(TSource source)
        {   
            return  Mapper.Map<TSource, TDestination>(source);
        }
    }
}