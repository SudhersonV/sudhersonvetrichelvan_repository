﻿using MovieSearch.Common.Entities;
using MovieSearch.Common.Enums;
using MovieSearch.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace MovieSearchWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IMovie
    {
        [OperationContract(Name="GetMovies")]
        [WebGet(UriTemplate = "GetMovies/{searchByContent}/{searchByGenre}", ResponseFormat = WebMessageFormat.Json)]
        List<TitleModel> GetMovies(string searchByContent, string searchByGenre);        

        [OperationContract]
        [WebGet(UriTemplate = "GetGenresList", ResponseFormat = WebMessageFormat.Json)]
        List<GenreModel> GetGenres();
    }
}
