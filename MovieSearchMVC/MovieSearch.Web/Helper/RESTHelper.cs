﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace MovieSearch.Web.Helper
{
    public enum RESTVerb
    {
        GET, POST, PUT, DELETE 
    }

    public class RESTHelper
    {
        public string EndPoint { get; set; }
        public RESTVerb Verb { get; set; }
        public string ContentType { get; set; }
        public string PostData { get; set; }

        public RESTHelper()
        {
            EndPoint = string.Empty;
            Verb = RESTVerb.GET;
            ContentType = "text/xml";
            PostData = string.Empty;
        }

        public RESTHelper(string endPoint)
        {
            EndPoint = endPoint;
            Verb = RESTVerb.GET;
            ContentType = "text/xml";
            PostData = string.Empty;
        }

        public RESTHelper(string endPoint, RESTVerb verb)
        {
            EndPoint = endPoint;
            Verb = verb;
            ContentType = "text/xml";
            PostData = string.Empty;
        }

        public RESTHelper(string endPoint, RESTVerb verb, string postData)
        {
            EndPoint = endPoint;
            Verb = verb;
            ContentType = "text/xml";
            PostData = postData;
        }

        public string InvokeREST()
        {
            return InvokeREST(string.Empty);
        }

        public string InvokeREST(string RESTParameter)
        {
            WebRequest request = WebRequest.Create(EndPoint + RESTParameter) as HttpWebRequest;
            request.Method = Verb.ToString();
            request.ContentLength = 0;
            request.ContentType = ContentType;

            string RESTResponse = string.Empty;

            if (!string.IsNullOrEmpty(PostData) && Verb == RESTVerb.POST)
            {
                byte[] postData = Encoding.Unicode.GetBytes(PostData);

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(postData, 0, PostData.Length);
                }
            }

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    string errorMessage = string.Format("Http request failed, received Httpcode {0}", response.StatusCode);
                    throw new ApplicationException(errorMessage);
                }

                using (Stream responseStream = response.GetResponseStream())
                {
                    using(StreamReader reader = new StreamReader(responseStream))
                    {
                        RESTResponse = reader.ReadToEnd();
                    }
                }
            }

            return RESTResponse;
        }
    }
}