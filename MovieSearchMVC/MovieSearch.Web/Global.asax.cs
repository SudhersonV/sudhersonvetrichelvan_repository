﻿using Microsoft.Practices.Unity;
using MovieSearch.Common.Abstract;
using MovieSearch.Web.Binders;
using MovieSearch.Web.Concrete;
using MovieSearch.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Unity.Mvc4;

namespace MovieSearch.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelBinders.Binders.Add(typeof(TitleSearchModel), new TitleSearchBinder());
            DependencyResolver.SetResolver(new UnityDependencyResolver(GetUnityContainer()));
        }


        static IUnityContainer GetUnityContainer()
        {
            IUnityContainer container = new UnityContainer();
            container.RegisterType<ITitleDataSource, TitleDataSource>();
            return container;
        }

    }
}