﻿using MovieSearch.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieSearch.Web.Binders
{
    public class TitleSearchBinder : IModelBinder
    {
        private const string sessionKey = "TitleSearchModel";

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            TitleSearchModel model = (TitleSearchModel)controllerContext.HttpContext.Session[sessionKey];
            if (model == null)
            {
                model = new TitleSearchModel();
                controllerContext.HttpContext.Session[sessionKey] = model;
            }
            return model;
        }
    }
}