﻿using MovieSearch.Common.Abstract;
using MovieSearch.Common.Entities;
using MovieSearch.Web.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace MovieSearch.Web.Concrete
{
    public class TitleDataSource : ITitleDataSource
    {
        private RESTHelper RestHelper;
        private JavaScriptSerializer deSer;

        public TitleDataSource()
        {
            RestHelper = new RESTHelper();
            deSer = new JavaScriptSerializer();
        }

        public List<TitleModel> GetTitleList(string searchByContent, string GenreList)
        {
            string restUrl = string.Empty;
            List<TitleModel> TitleModel;

            restUrl = ConfigurationManager.AppSettings["RestGetMovies"]
                .Replace("{searchByContent}", searchByContent == string.Empty ? "ALL" : searchByContent)
                .Replace("{searchByGenre}", GenreList);

            TitleModel = deSer.Deserialize<List<TitleModel>>(RestHelper.InvokeREST(restUrl));

            return TitleModel;
        }

        public List<GenreModel> GetGenreList()
        {
            string restURl = string.Empty;
            List<GenreModel> genreList = new List<GenreModel>();

            restURl = ConfigurationManager.AppSettings["RestGetGenre"];
            genreList = deSer.Deserialize<List<GenreModel>>(RestHelper.InvokeREST(restURl));
            genreList.Insert(0, new GenreModel { Id = 0, Name = "ALL" });

            return genreList;
        }
    }
}
