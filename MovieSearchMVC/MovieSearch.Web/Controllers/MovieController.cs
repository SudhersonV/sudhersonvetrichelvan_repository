﻿using MovieSearch.Web.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using MovieSearch.Common.Entities;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using MovieSearch.Web.Models;
using MovieSearch.Common.Enums;
using System.Web.UI;
using MovieSearch.Web.Concrete;
using MovieSearch.Common.Abstract;

namespace MovieSearch.Web.Controllers
{
    public class MovieController : Controller
    {
        private ITitleDataSource IdataSource;

        public MovieController(ITitleDataSource IDataSource)
        {
            IdataSource = IDataSource;
        }

        [HttpGet]
        public ActionResult Search(TitleSearchModel titleSearchModel)
        {
            List<GenreModel> genreList = new List<GenreModel>();

            genreList = IdataSource.GetGenreList();
            titleSearchModel.showSearchResult = false;
            titleSearchModel.AddGenreList(genreList);
            titleSearchModel.isReturnToResult = false;

            return View(titleSearchModel);
        }


        public PartialViewResult TitleListPartialView(TitleSearchModel titleSearchModel, string searchByContent, string GenreList = "ALL")
        {
            if (titleSearchModel.showSearchResult)
            {
                if (!titleSearchModel.isReturnToResult)
                {
                    titleSearchModel.AddTitleList(IdataSource.GetTitleList(searchByContent, GenreList));
                }
                titleSearchModel.isReturnToResult = false;
            }
            titleSearchModel.showSearchResult = true;
            return PartialView(titleSearchModel);
        }

        [HttpPost]
        public ActionResult Search(TitleSearchModel titleSearchModel, string searchByContent, string GenreList)
        {   
            titleSearchModel.isReturnToResult = true;
            return View(titleSearchModel);
        }

        public ActionResult Awards(TitleSearchModel titleSearchModel, string id)
        {
            List<AwardModel> AwardList = new List<AwardModel>();
            AwardList = titleSearchModel.TitleList.First(l => l.TitleId.ToString() == id).Awards;
            titleSearchModel.isReturnToResult = true;
            return View(AwardList);
        }

        public ActionResult OtherNames(TitleSearchModel titleSearchModel, string id)
        {
            List<OtherNameModel> OtherNameList = new List<OtherNameModel>();
            OtherNameList = titleSearchModel.TitleList.First(l => l.TitleId.ToString() == id).OtherNames;
            titleSearchModel.isReturnToResult = true;
            return View(OtherNameList);
        }

        public ActionResult StoryLines(TitleSearchModel titleSearchModel, string id)
        {
            List<StoryLineModel> StoryLineList = new List<StoryLineModel>();
            StoryLineList = titleSearchModel.TitleList.First(l => l.TitleId.ToString() == id).StoryLines;
            titleSearchModel.isReturnToResult = true;
            return View(StoryLineList);
        }

        public ActionResult TitleGenres(TitleSearchModel titleSearchModel, string id)
        {
            List<TitleGenreModel> TitleGenreList = new List<TitleGenreModel>();
            TitleGenreList = titleSearchModel.TitleList.First(l => l.TitleId.ToString() == id).TitleGenres;
            titleSearchModel.isReturnToResult = true;
            return View(TitleGenreList);
        }

        public ActionResult TitleParticipants(TitleSearchModel titleSearchModel, string id)
        {
            List<TitleParticipantModel> TitleParticipantList = new List<TitleParticipantModel>();
            TitleParticipantList = titleSearchModel.TitleList.First(l => l.TitleId.ToString() == id).TitleParticipants;
            titleSearchModel.isReturnToResult = true;
            return View(TitleParticipantList);
        }
    }
}
