﻿using MovieSearch.Common.Entities;
using MovieSearch.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieSearch.Web.Models
{
    public class TitleSearchModel
    {
        public List<TitleModel> TitleList;
        public string SearchByContent;
        public string SearchByGenre;
        public List<GenreModel> GenreList;
        public bool showSearchResult = false;
        public bool isReturnToResult;

        public void AddTitleList(List<TitleModel> titleList)
        {
            TitleList = titleList;
        }

        public void AddGenreList(List<GenreModel> genreList)
        {
            GenreList = genreList;
        }
    }
}