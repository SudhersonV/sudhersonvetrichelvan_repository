﻿using MovieSearch.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieSearch.Common.Abstract
{
    public interface ITitleDataSource
    {
        List<TitleModel> GetTitleList(string searchByContent, string GenreList);
        List<GenreModel> GetGenreList();
    }
}
