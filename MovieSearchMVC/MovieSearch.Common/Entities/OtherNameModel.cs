﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MovieSearch.Common.Entities
{
    [DataContract]
    public class OtherNameModel
    {
        [DataMember]
        public Nullable<int> TitleId { get; set; }
        [DataMember]
        public string TitleNameLanguage { get; set; }
        [DataMember]
        public string TitleNameType { get; set; }
        [DataMember]
        public string TitleNameSortable { get; set; }
        [DataMember]
        public string TitleName { get; set; }
        [DataMember]
        public int Id { get; set; }
    }
}
