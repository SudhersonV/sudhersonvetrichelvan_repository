﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MovieSearch.Common.Entities
{
    [DataContract]
    public class GenreModel
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }

        //[DataMember]
        //public List<TitleGenreModel> TitleGenres { get; set; }
    }
}
