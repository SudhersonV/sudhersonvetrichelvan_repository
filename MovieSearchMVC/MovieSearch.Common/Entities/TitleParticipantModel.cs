﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MovieSearch.Common.Entities
{
    [DataContract]
    public class TitleParticipantModel
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int TitleId { get; set; }
        [DataMember]
        public int ParticipantId { get; set; }
        [DataMember]
        public bool IsKey { get; set; }
        [DataMember]
        public string RoleType { get; set; }
        [DataMember]
        public bool IsOnScreen { get; set; }
        //[DataMember]
        //public ParticipantModel Participant { get; set; }
    }
}
