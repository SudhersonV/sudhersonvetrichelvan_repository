﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MovieSearch.Common.Entities
{
    [DataContract] 
    public class AwardModel
    {
        [DataMember]
        public int TitleId { get; set; }
        [DataMember]
        public Nullable<bool> AwardWon { get; set; }
        [DataMember]
        public Nullable<int> AwardYear { get; set; }
        [DataMember]
        public string Award1 { get; set; }
        [DataMember]
        public string AwardCompany { get; set; }
        [DataMember]
        public int Id { get; set; }
    }
}
