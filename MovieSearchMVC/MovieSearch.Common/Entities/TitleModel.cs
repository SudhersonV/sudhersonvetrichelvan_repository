﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MovieSearch.Common.Entities
{
    [DataContract]    
    public class TitleModel
    {
        [DataMember]
        public int TitleId { get; set; }
        [DataMember]
        public string TitleName { get; set; }
        [DataMember]
        public string TitleNameSortable { get; set; }
        [DataMember]
        public Nullable<int> TitleTypeId { get; set; }
        [DataMember]
        public Nullable<int> ReleaseYear { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ProcessedDateTimeUTC { get; set; }

        [DataMember]
        public List<AwardModel> Awards { get; set; }
        [DataMember]
        public List<OtherNameModel> OtherNames { get; set; }
        [DataMember]
        public List<StoryLineModel> StoryLines { get; set; }
        [DataMember]
        public List<TitleGenreModel> TitleGenres { get; set; }
        [DataMember]
        public List<TitleParticipantModel> TitleParticipants { get; set; }
    }
}
