﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MovieSearch.Common.Entities
{
    [DataContract]
    public class ParticipantModel
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ParticipantType { get; set; }

        //public List<TitleParticipantModel> TitleParticipants { get; set; }
    }
}
