﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MovieSearch.Common.Entities
{
    [DataContract]
    public class TitleGenreModel
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int TitleId { get; set; }
        [DataMember]
        public int GenreId { get; set; }
        [DataMember]
        public GenreModel Genre { get; set; }
    }
}
