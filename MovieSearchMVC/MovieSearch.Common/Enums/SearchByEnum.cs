﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieSearch.Common.Enums
{
    public enum SearchByEnum
    {
        BeginsWith, Contains, EndWith
    }
}
